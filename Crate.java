import java.util.Scanner;

/**
 * An abstract class that represents types of shipping Crates. Implements the
 * 	Package interface.
 * 
 * @author 5153
 * @version 1.0, 10 Oct 2017
 */
public abstract class Crate implements Package {
	public double weight; // weight of the Crate, used to calculate cost
	
	/**
	* Requests the user input the weight of the crate.
	* 
	* @param Scanner scanner input object
	*/
	@Override 
	public void input(Scanner scanner) {
		
		// Class simple name is used to identify the type (subclass) of crate
		System.out.printf("Please input the weight for the %s (lbs) : ",
				getClass().getSimpleName()); 
		weight = Double.parseDouble(scanner.nextLine());
	}
}