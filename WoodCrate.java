/**
 * Crate subclass that represents a wood shipping crate.
 * 
 * @author 5153
 * @version 1.0, 10 Oct 2017
 */
public class WoodCrate extends Crate {
	
	/**
	 * Calculates the dollar amount to ship this wood crate.
	 * 
	 * @return Double cost to ship this wood crate
	 */
	@Override 
	public double cost() {
		return weight * 1.4;  // $1.40 per lb.
	}
}