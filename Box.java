import java.util.Scanner;

/**
 * Class representing a box to be shipped. Implements the Package interface.
 * 
 * @author 5153
 * @version 1.0, 10 Oct 2017
 */
public class Box implements Package {
	private double weight; // weight of the box, used to calculate cost
	
	/**
	 * Calculates the dollar amount to ship this box.
	 * 
	 * @return Double cost to ship this box
	 */
	@Override 
	public double cost() {
		return weight * 1.2;  // $1.20 per lb.
	}
	
	/**
	* Requests the user input the weight of the box to ship.
	* 
	* @param Scanner scanner input object
	*/
	@Override 
	public void input(Scanner scanner) {
		System.out.printf("Please input the weight for the box (lbs) : ");
		weight = Double.parseDouble(scanner.nextLine());
	}
}