import java.util.Scanner;

/**
 * Class representing a letter to be shipped. Implements the Package interface.
 * 
 * @author 5153
 * @version 1.0, 10 Oct 2017
 */
public class Letter implements Package {
	private int numOfPages; // pages being shipped, used to calculate cost
	
	/**
	 * Calculates the dollar amount to ship this letter.
	 * 
	 * @return Double cost to ship this letter
	 */
	@Override 
	public double cost() {
		return numOfPages * 0.05;  // $0.05 per page
	}
	
	/**
	* Requests the user input the number of pages in the letter.
	* 
	* @param Scanner scanner input object
	*/
	@Override 
	public void input(Scanner scanner) {
		System.out.print(
				"Please input the number of pages for the letter (pgs): ");
		numOfPages = Integer.parseInt(scanner.nextLine());
	}
}