import java.util.Scanner;

/**
 * An interface that represents a package for customer delivery.
 * 
 * @author 5153
 * @version 1.0, 10 Oct 2017
 */
public interface Package {

    double cost();	// determine cost for the package
    void input(Scanner scanner);  // input data for the package
}