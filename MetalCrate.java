/**
 * Crate subclass that represents a metal shipping crate.
 * 
 * @author 5153
 * @version 1.0, 10 Oct 2017
 */
public class MetalCrate extends Crate {
	
	/**
	 * Calculates the dollar amount to ship this metal crate.
	 * 
	 * @return Double cost to ship this metal crate
	 */
	@Override 
	public double cost() {
		return weight * 1.3;  // $1.30 per lb.
	}
}